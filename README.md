# Workshop

## Tranformaciones

### Capa Staging

#### Customers
1. **Subconsulta Fuente:**
   - Crea una subconsulta llamada `source`, seleccionando todos los campos de la tabla `CUSTOMERS`.

2. **Subconsulta Renombramiento:**
   - Genera una subconsulta `renamed` que transforme los datos obtenidos de `source`. Debes:
     - Renombra el campo `id` a `customer_id`.
     - Renombra el campo `name` a `customer_name`.

3. **Consulta Final:**
   - Realica una consulta final que seleccione todos los campos de la subconsulta `renamed`.

#### Items
1. **Subconsulta Fuente:**
   - Crea una subconsulta llamada `source`, que seleccione todos los campos de la tabla `ITEMS`.

2. **Subconsulta Renombramiento:**
   - Elabora una subconsulta `renamed` que utilice los datos de `source`. En esta subconsulta, debes:
     - Renombra el campo `id` a `item_id`.
     - Incluir el campo `order_id` tal cual.
     - Renombra el campo `sku` a `product_id`.

3. **Consulta Final:**
   - Realica una consulta que seleccione todos los campos de la subconsulta `renamed`.

#### Orders
1. **Subconsulta Fuente:**
   - Crea una subconsulta llamada `source`, que seleccione todos los campos de la tabla `ORDERS`.

2. **Subconsulta Renombramiento:**
   - Elabora una subconsulta `renamed` que utilice los datos de `source`. En esta subconsulta, debes:
     - Renombra el campo `id` a `order_id`.
     - Renombra el campo `store_id` a `location_id`.
     - Renombra el campo `customer` a `customer_id`.
     - Formatear el campo `ordered_at` a  `'DD/MM/YYYY HH24:MI:SS`
     - Incluir los campos campo `subtotal`, `subtotal` y `order_total` tal cual.
     - Renombra el campo `sku` a `product_id`.

3. **Consulta Final:**
   - Realica una consulta que seleccione todos los campos de la subconsulta `renamed`.

#### Products

1. **Subconsulta Fuente:**
   - Crea una subconsulta llamada `source`, que seleccione todos los campos de la tabla `ORDERS`.

2. **Subconsulta Renombramiento:**
   - Elabora una subconsulta `renamed` que utilice los datos de `source`. En esta subconsulta, debes:
     - Renombra el campo `id` a `order_id`.
     - Renombra el campo `store_id` a `location_id`.
     - Renombra el campo `customer` a `customer_id`.
     - Formatear el campo `ordered_at` a  `'DD/MM/YYYY HH24:MI:SS`
     - Incluir los campos `subtotal`, `subtotal` y `order_total` tal cual.
     - Renombra el campo `sku` a `product_id`.

3. **Consulta Final:**
   - Realica una consulta que seleccione todos los campos de la subconsulta `renamed`.

#### Stores
1. **Subconsulta Fuente:**
   - Crea una subconsulta llamada `source`, que seleccione todos los campos de la tabla `STORES`.

2. **Subconsulta Renombramiento:**
   - Elabora una subconsulta `renamed` que utilice los datos de `source`. En esta subconsulta, debes:
     - Renombra el campo `id` a `store_id`.
     - Renombra el campo `name` a `store_name`.
     - Incluir el campo `tax_rate` tal cual.
     - Formatear el campo `opened_at` a  `'DD/MM/YYYY HH24:MI:SS`

3. **Consulta Final:**
   - Realica una consulta que seleccione todos los campos de la subconsulta `renamed`.

#### Supplies
1. **Subconsulta Fuente:**
   - Crea una subconsulta llamada `source`, que seleccione todos los campos de la tabla `SUPPLIES`.

2. **Subconsulta Renombramiento:**
   - Elabora una subconsulta `renamed` que utilice los datos de `source`. En esta subconsulta, debes:
     - Genera un campo `supply_uuid` utilizando la función `dbt_utils.generate_surrogate_key` combinando los campos `id` y `sku`.
     - Renombra el campo `id` a `supply_id`.
     - Renombra el campo `sku` a `product_id`.
     - Renombra el campo `name` a `supply_name`.
     - Formatear el campo `perishable` a  `is_perishable_supply`

3. **Consulta Final:**
   - Realica una consulta que seleccione todos los campos de la subconsulta `renamed`.

### Capa Intermediate

#### Orders
##### Parte 1: Preparación de Datos

1. **Subconsulta de Órdenes (`orders`):**
   - Crea una subconsulta denominada `orders` que seleccione todos los campos de la tabla de origen `stg_orders`. Utiliza la función `{{ ref('stg_orders') }}` de DBT para referenciar la tabla.

2. **Subconsulta de Artículos (`items`):**
   - Define una subconsulta llamada `items` seleccionando todos los campos de la tabla `stg_items`, empleando la función `{{ ref('stg_items') }}` para hacer la referencia adecuada.

##### Parte 2: Análisis de Datos

3. **Construcción de la Subconsulta de Resumen de Órdenes (`order_summary`):**
   - Utilizando las subconsultas `orders` e `items`, crea una nueva subconsulta `order_summary`. En esta sección, debes realizar lo siguiente:
     - Agrupa los datos por `customer_id`.
     - Calcula el total de órdenes únicas por cliente con `COUNT(DISTINCT order_id)` y almacénalo en `total_orders`.
     - Determina si el cliente es un comprador repetido (`is_repeat_buyer`), esto es, si ha realizado más de una orden.
     - Identifica la fecha de la primera (`first_ordered_at`) y última orden (`last_ordered_at`) por cliente.
     - Calcula el gasto total antes de impuestos (`lifetime_spend_pretax`) y el gasto total (`lifetime_spend`) por cliente.
   - Asegúrate de hacer un `LEFT JOIN` de `orders` con `items` basándote en `order_id` para incluir todos los registros relevantes.

##### Parte 3: Consulta Final

4. **Selección Final:**
   - Realiza una consulta final que seleccione todos los campos de la subconsulta `order_summary`. Esta consulta debe proporcionar una visión completa del comportamiento de compra de los clientes, incluyendo tanto las métricas financieras como las de fidelización.

#### Supplies

##### Parte 1: Preparación de Datos

1. **Subconsulta de Suministros (`supplies`):**
   - Crea una subconsulta llamada `supplies` que seleccione todos los campos de la tabla de origen `stg_supplies`. Utiliza la función `{{ ref('stg_supplies') }}` de DBT para referenciar la tabla.

##### Parte 2: Análisis de Datos

2. **Construcción de la Subconsulta de Resumen de Suministros (`supplies_summary`):**
   - Utilizando la subconsulta `supplies`, crea una nueva subconsulta denominada `supplies_summary`. En esta subconsulta, debes:
     - Agrupar los datos por `product_id`.
     - Calcular la suma del costo del suministro (`supply_cost`) por producto con `SUM(supply_cost)`.

##### Parte 3: Consulta Final

3. **Selección Final:**
   - Realiza una consulta final que seleccione todos los campos de la subconsulta `supplies_summary`. Esta consulta proporcionará un resumen del costo total del suministro para cada producto.


### Capa Marts

#### Customers

##### Parte 1: Preparación de Datos

1. **Subconsulta de Clientes (`customers`):**
   - Crea una subconsulta llamada `customers` que seleccione todos los campos de la tabla `int_customers` utilizando `{{ ref('int_customers') }}` para referenciar la tabla.

2. **Subconsulta de Órdenes (`orders`):**
   - Define una subconsulta `orders` seleccionando todos los campos de `int_orders` con `{{ ref('int_orders') }}`.

##### Parte 2: Creación de la Vista de Clientes (`mart_customers`)

3. **Construcción de la Vista `mart_customers`:**
   - Utilizando las subconsultas `customers` y `orders`, crea una vista denominada `mart_customers`. En esta vista, debes:
     - Incluir todos los campos de `customers`.
     - Agregar los campos relacionados con las órdenes: `total_orders`, `first_ordered_at`, `last_ordered_at`, `lifetime_spend_pretax`, y `lifetime_spend`.
     - Clasificar a los clientes en 'returning' o 'new' basándote en si son compradores recurrentes (`is_repeat_buyer`), usando una sentencia `CASE`.

##### Parte 3: Consulta Final

4. **Selección Final:**
   - Realiza una consulta final que seleccione todos los campos de la vista `mart_customers`. Esta vista debe proporcionar un perfil completo de cada cliente, enriquecido con detalles relevantes de sus órdenes.


#### Items

##### Parte 1: Preparación de Datos

1. **Subconsulta de Suministros (`supplies`):**
   - Crea una subconsulta llamada `supplies` seleccionando todos los campos de la tabla `int_supplies` usando `{{ ref('int_supplies') }}` para referenciar.

2. **Subconsulta de Artículos (`items`):**
   - Define una subconsulta `items` seleccionando todos los campos de `int_items` con `{{ ref('int_items') }}`.

3. **Subconsulta de Productos (`products`):**
   - Establece una subconsulta `products` seleccionando todos los campos de `int_products` mediante `{{ ref('int_products') }}`.

4. **Subconsulta de Órdenes (`orders`):**
   - Crea una subconsulta `orders` seleccionando todos los campos de `stg_orders` utilizando `{{ ref('stg_orders') }}`.

##### Parte 2: Creación de la Vista `mart_items`

5. **Construcción de la Vista `mart_items`:**
   - Utilizando las subconsultas anteriores, crea una vista llamada `mart_items`. Debes realizar lo siguiente:
     - Selecciona `item_id` de `items`.
     - Incluye `product_price` de `products` y `supply_cost` de `supplies`.
     - Agrega campos `is_food_item` y `is_drink_item` de `products`.
     - Usa `LEFT JOIN` para combinar `items` con `orders`, `products` y `supplies` basándote en `order_id` y `product_id` respectivamente.

##### Parte 3: Consulta Final

6. **Selección Final:**
   - Realiza una consulta final que seleccione todos los campos de la vista `mart_items`. Esta consulta debe proporcionar una visión detallada de los ítems, incluyendo información sobre precios y costos, así como su clasificación.