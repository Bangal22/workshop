WITH 

orders AS (
    SELECT * FROM {{ ref('stg_orders') }}
),

items AS (
    SELECT * FROM {{ ref('stg_items') }}
),

order_summary AS (
    SELECT 
        orders.customer_id, 
        COUNT(DISTINCT orders.order_id) AS total_orders,
        COUNT(DISTINCT orders.order_id) > 1 AS is_repeat_buyer,
        MIN(orders.ordered_at) AS first_ordered_at,
        MAX(orders.ordered_at) AS last_ordered_at,
        SUM(orders.subtotal) AS lifetime_spend_pretax,
        SUM(orders.order_total) as lifetime_spend
    FROM orders
    LEFT JOIN items ON items.order_id = orders.order_id
    GROUP BY orders.customer_id
) 

SELECT * FROM order_summary