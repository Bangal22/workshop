WITH 

supplies AS (
    SELECT * FROM {{ ref('stg_supplies') }}
),

supplies_summary AS (
    SELECT
        product_id,
        SUM(supply_cost) AS supply_cost
    FROM supplies
    GROUP BY product_id
)

SELECT * FROM supplies_summary