WITH 

source AS (
    SELECT * FROM {{ source('WORKSHOP_SOURCE', 'ITEMS') }}
),

renamed AS (
    SELECT 
        id AS item_id, 
        order_id,
        sku as product_id
    FROM source
)

SELECT * FROM renamed