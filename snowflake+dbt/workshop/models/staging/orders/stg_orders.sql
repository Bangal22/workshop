WITH 

source AS (
    SELECT * FROM {{ source('WORKSHOP_SOURCE', 'ORDERS') }}
),

renamed AS (
    SELECT 
        id as order_id,
        store_id as location_id,
        customer as customer_id,
        TO_CHAR(TO_DATE(ordered_at), 'DD/MM/YYYY HH24:MI:SS') as ordered_at,
        subtotal,
        order_total
    FROM source
)

SELECT * FROM renamed