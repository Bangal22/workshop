WITH 

source AS (
    SELECT * FROM {{ source('WORKSHOP_SOURCE', 'SUPPLIES') }}
),

renamed AS (
    SELECT 
        {{ dbt_utils.generate_surrogate_key(['id', 'sku']) }} AS supply_uuid,
        id AS supply_id,
        sku AS product_id,
        name AS supply_name,
        cost AS supply_cost,
        perishable AS is_perishable_supply
    FROM source
)

SELECT * FROM renamed