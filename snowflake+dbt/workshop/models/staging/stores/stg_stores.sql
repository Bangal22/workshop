WITH 

source AS (
    SELECT * FROM {{ source('WORKSHOP_SOURCE', 'STORES') }}
),

renamed AS (
    SELECT 
        id AS store_id,
        name AS store_name,
        tax_rate,
        TO_CHAR(TO_DATE(opened_at), 'DD/MM/YYYY HH24:MI:SS') as opened_at
    FROM source
)

SELECT * FROM renamed