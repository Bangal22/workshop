WITH 

source AS (
    SELECT * FROM {{ source('WORKSHOP_SOURCE', 'CUSTOMERS') }}
),

renamed AS (
    SELECT 
        id AS customer_id, 
        name AS customer_name
    FROM source
)

SELECT * FROM renamed