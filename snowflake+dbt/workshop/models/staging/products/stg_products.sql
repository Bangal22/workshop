WITH 

{% set is_food_item = "type = 'jaffle'" %}
{% set is_drink_item = "type = 'beverage'" %}

source AS (
    SELECT * FROM {{ source('WORKSHOP_SOURCE', 'PRODUCTS') }}
),

renamed AS (
    SELECT 
        sku as product_id,
        name as product_name,
        type as product_type,
        description as product_description,
        price as product_price,
        {% if is_food_item %} true {% else %} false {% endif %} as is_food_item,
        {% if is_drink_item %} true {% else %} false {% endif %} as is_drink_item
        -- coalesce(type = 'jaffle', false) as is_food_item,
        -- coalesce(type = 'beverage', false) as is_drink_item
    FROM source
)

SELECT * FROM renamed