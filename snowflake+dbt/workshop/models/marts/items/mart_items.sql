WITH

supplies AS (
    SELECT * FROM {{ ref('int_supplies') }}
),

items AS (
    SELECT * FROM {{ ref('int_items') }}
),

products AS (
    SELECT * FROM {{ ref('int_products') }}
),

orders AS (
    SELECT * FROM {{ ref('stg_orders') }}
),

mart_items as (
    SELECT
        items.item_id,
        products.product_price,
        supplies.supply_cost,
        products.is_food_item,
        products.is_drink_item
    FROM items

    LEFT JOIN orders ON items.order_id = orders.order_id
    LEFT JOIN products ON items.product_id = products.product_id
    LEFT JOIN supplies ON items.product_id = supplies.product_id
)

SELECT * FROM mart_items