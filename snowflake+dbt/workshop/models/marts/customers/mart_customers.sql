with

customers AS (
    SELECT * FROM {{ ref('int_customers') }}
),

orders AS (
    SELECT * FROM {{ ref('int_orders') }}
),

mart_customers AS (
    SELECT
        customers.*,
        orders.total_orders,
        orders.first_ordered_at,
        orders.last_ordered_at,
        orders.lifetime_spend_pretax,
        orders.lifetime_spend,
        CASE
            WHEN orders.is_repeat_buyer THEN 'returning'
            ELSE 'new'
        END AS customer_type
    FROM customers
    LEFT JOIN orders ON customers.customer_id = orders.customer_id
)

SELECT * FROM mart_customers